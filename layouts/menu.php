<?php
session_start();
$username = $_SESSION['username'];
// db
require_once ('classes/config.php');

$sql = "SELECT * from users WHERE user_name = :username";
$params = array(
    'username' => $username
);
$stmt = $connect->prepare($sql);
$stmt->execute($params);
$result = $stmt->fetch(PDO::FETCH_ASSOC);

?>
<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
    <a class="navbar-brand" href="./">Cars</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="./"><i class="fa fa-home mr-2"></i>Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">About</a>
            </li>

        </ul>
        <div class="float-right">
         <ul class="navbar-nav mr-auto">
             <?php if($_SESSION['login'] == true){ ?>

             <li class="nav-item">
                 <a class="nav-link" href="./profile.php">Profile</a>
             </li>
                 <li class="nav-item">

                     <a class="nav-link" href="./profile.php"><i class="fa fa-user-check text-success ml-2"></i><span class="text-success"><?php echo $result['user_name']; ?></span></a>

                 </li>

                 <li class="nav-item ml-3">
                     <a class="nav-link" href="./logout.php">Logout</a>
                 </li>

             <?php }else{ ?>
             <li class="nav-item">
                 <a class="nav-link" href="./login.php">Login</a>
             </li>
             <li class="nav-item">
                 <a class="nav-link" href="./register.php">Register</a>
             </li>
             <?php } ?>
        </ul>
        </div>
    </div>
</nav>