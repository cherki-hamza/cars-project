<?php
session_start();
// db
require_once ('classes/config.php');
$title = 'Register Page';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

// get data from register form
    $user_name = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);




        // insert user to database
        $sql = "INSERT INTO users(user_name,email,password) values (:user_name ,:email,:password)";
        $stmt = $connect->prepare($sql);
        $result = $stmt->execute(array(
            ':user_name' => $username,
            ':email' => $email,
            ':password' => $password
            )
        );

        if ( $result === true){
            $_SESSION['message'] ="<span class='text-success'>you are register with success</span>";
            header('location: login.php');
            session_write_close();
            exit();
        }else{
            $error = true;
            $_SESSION['message'] = '<span class="text-danger">Oops error DB and sql </span>';
        }


}

?>

<!-- start header -->
<?php include_once ('layouts/header.php'); ?>
<!-- end header -->

<!-- start content -->
<div class="container-fluid">
    <?php include_once ('layouts/menu.php'); ?>
</div>

<div class="container">
    <div class="col-8 justify-content-center">
        <div class="card text-center my-5">
           <div class="card-header">
                <h1 class="text-primary">Register</h1>
           </div>
            <div class="card-body">
                <form action="register.php" method="post">
                    <div class="row form-group">
                        <label for="username">Username :</label>
                        <input type="text" class="form-control" name="username" id="username" placeholder="Write your username">
                    </div>

                    <div class="row form-group">
                        <label for="username">Email :</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Write your email">
                    </div>

                    <div class="row form-group">
                        <label for="password">Password :</label>
                        <input type="password" class="form-control" name="password" id="password">
                    </div>

                    <div class="row form-group">
                        <input type="submit" class="btn btn-primary" value="Register">
                        <small class="text-center ml-3 text-success"><a href="login.php">Or login to your Profile</a></small>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- end content -->


<!-- start footer -->
<?php include_once ('layouts/footer.php'); ?>
<!-- end footer -->




