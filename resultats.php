<?php
session_start();
$username = $_SESSION['username'];
// db
require_once ('classes/config.php');
$title = 'Results Page';
?>

<!-- start header -->
<?php include_once ('layouts/header.php'); ?>
<!-- end header -->

<!-- start content -->
<div class="container-fluid">
    <?php include_once ('layouts/menu.php'); ?>
</div>

<div class="container">
    <div class="jumbotron jumbotron-fluid mt-5">
        <div class="container">
            <h1 class="display-4 my-5">Welcome to cars management system</h1>

            <form class="form-inline mt-5">
                <div class="row">
                    <div class="form-group ml-5 mr-5">
                        <label for="marque"><span class="text-primary">marque :</span></label>
                        <select class="form-control form-control-sm ml-3">
                            <option>marque</option>
                        </select>
                    </div>
                    <div class="form-group ml-5">
                        <label for="region"><span class="text-primary">Region :</span></label>
                        <select class="form-control form-control-sm ml-3">
                            <option>Small select</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-primary mb-2 ml-5">Get the Car by cat and region </button>
                </div>
            </form>

        </div>
    </div>
</div>

<!-- end content -->


<!-- start footer -->
<?php include_once ('layouts/footer.php'); ?>
<!-- end footer -->


