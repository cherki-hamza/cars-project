<?php
session_start();
// db
require_once ('classes/config.php');
$title = "Profile Page";

// get user profile.png

$sql = "SELECT * from users WHERE user_name = :username";
$params = array(
    'username' => $username
);
$stmt = $connect->prepare($sql);
$stmt->execute($params);
$profile = $stmt->fetch(PDO::FETCH_ASSOC);


?>

<!-- start header -->
<?php include_once ('layouts/header.php'); ?>
<!-- end header -->

<!-- start content -->
<div class="container-fluid">
    <?php include_once ('layouts/menu.php'); ?>
</div>

<div class="container mt-5">
 <h1 class="text-primary my-2"><?php echo $username; ?> Profile</h1>
  <div class="card my-2">
      <div class="card" style="width: 20rem;">
          <img src="assets/img/profile.png" class="card-img-top" alt="...">
          <div class="card-body">
              <h5 class="card-title"> Name :  <?php echo $profile['user_name'];?></h5>
              <p class="card-text"> Email :  <?php echo $profile['email'];?></p>
          </div>
          <ul class="list-group list-group-flush">
              <li class="list-group-item"><?php  ?></li>
              <li class="list-group-item"> Created_at :  <?php echo $profile['created_at'];?></li>

          </ul>
          <div class="card-body">
              <a href="profile.php" class="card-link">Profile Info</a>
          </div>
      </div>
  </div>
</div>

<!-- end content -->


<!-- start footer -->
<?php include_once ('layouts/footer.php'); ?>
<!-- end footer -->


