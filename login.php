<?php
session_start();
$login =false;
// db
require_once ('classes/config.php');
$title = "Login Page";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

// get data from login form
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

// select users from database
   $sql = "SELECT * from users WHERE user_name = :username AND password = :password ";

    $params = array(
        'username' => $username,
        'password' => $password
    );

    $stmt = $connect->prepare($sql);
    $stmt->execute($params);
    $result = $stmt->fetch(PDO::FETCH_ASSOC);



    if ( $result){
        $_SESSION['message'] ="<span class='text-success'>$username hello</span>";
        $_SESSION['username'] = $username;
        $_SESSION['login'] = true;
        header('location: index.php');
        session_write_close();
        exit();
    }else{
        $_SESSION['message'] ="<span class='text-success'>your username or password is incorrect</span>";
        header('location: login.php');
    }



}

?>

<!-- start header -->
<?php include_once ('layouts/header.php'); ?>
<!-- end header -->

<!-- start menu -->
<div class="container-fluid">
    <?php include_once ('layouts/menu.php'); ?>
</div>
<!-- end menu -->

<!-- start content -->
<div class="container ">
    <div class="col-md-8">
        <div class="card text-center my-5 justify-content-center">
            <div class="card-header">
                <h1 class="text-primary">Login</h1>
            </div>
            <div class="card-body">
                <form action="login.php" method="POST">
                    <div class="row form-group">
                        <label for="username">Username :</label>
                        <input type="text" class="form-control" name="username" id="username" placeholder="Write your username">
                    </div>

                    <div class="row form-group">
                        <label for="password">Password :</label>
                        <input type="password" class="form-control" name="password" id="password">
                    </div>

                    <div class="row form-group">
                        <input type="submit" class="btn btn-success" value="Login">

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- end content -->


<!-- start footer -->
<?php include_once ('layouts/footer.php'); ?>
<!-- end footer -->


