<?php
session_start();
$username = $_SESSION['username'];
$title = "Home | cars";
// db
require_once ('classes/config.php');

// select user by login user by session
$sql = "SELECT * from users WHERE user_name = :username";
$params = array(
    'username' => $username
);
$stmt = $connect->prepare($sql);
$stmt->execute($params);
$result = $stmt->fetch(PDO::FETCH_ASSOC);

// select marque
$sql2 = "SELECT * from marques";
$stmt = $connect->prepare($sql2);
$stmt->execute();
$marques = $stmt->fetchAll(PDO::FETCH_ASSOC);

// get the region
$sql3 = "SELECT * FROM cars";
$stmt = $connect->prepare($sql3);
$stmt->execute();
$regions = $stmt->fetchAll(PDO::FETCH_ASSOC);



?>

<!-- start header -->
<?php include_once ('layouts/header.php'); ?>
<!-- end header -->

<!-- start content -->
<div class="container-fluid">
    <?php include_once ('layouts/menu.php'); ?>
</div>

<?php if(isset($username) && $_SESSION['login'] == true){ ?>
<div class="container">
    <div class="jumbotron jumbotron-fluid mt-5">
        <div class="container">
            <h1 class="display-4 my-5">Welcome to cars management system</h1>

            <form action="resultats.php" method="POST" class="form-inline mt-5">
                <div class="row">
                <div class="form-group ml-5 mr-5">
                    <label for="marque"><span class="text-primary">marque :</span></label>
                    <select name="marque" id="marque" class="form-control form-control-sm ml-3">
                        <?php foreach ($marques as $marque){ ?>
                        <option value="<?php echo $marque['id']; ?>"><?php echo $marque['marque_name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
                    <div class="form-group ml-5">
                        <label for="region"><span class="text-primary">Region :</span></label>
                        <select name="region" id="region" class="form-control form-control-sm ml-3">
                            <?php foreach ($regions as $region){ ?>
                                <option value="<?php echo $regions['city']; ?>"><?php echo $region['city']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                <div class="form-group float-right">
                    <button type="submit" class="btn btn-primary mb-2 ml-5"><i class="fa fa-search text-danger mr-4"></i>Get the Car by Marque and region </button>
                </div>

                </div>
            </form>

        </div>
    </div>
</div>
<?php }else{ ?>
    <div class="container">
    <div class="jumbotron jumbotron-fluid mt-5">
        <div class="container">
            <h1 class="display-4 my-5">Welcome to cars management system</h1>
            <h3 class="text-center aler alert-danger">You have to register new user to show the cars Dashboard</h3>
    </div>
    </div>
<?php } ?>
<!-- end content -->


<!-- start footer -->
<?php include_once ('layouts/footer.php'); ?>
<!-- end footer -->


